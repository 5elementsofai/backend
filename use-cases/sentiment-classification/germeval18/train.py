import torch
from farm.modeling.tokenization import Tokenizer
from farm.data_handler.processor import TextClassificationProcessor
from farm.data_handler.data_silo import DataSilo
from farm.modeling.language_model import LanguageModel
from farm.modeling.prediction_head import TextClassificationHead
from farm.modeling.adaptive_model import AdaptiveModel
from farm.modeling.optimization import initialize_optimizer
from farm.train import Trainer
from farm.utils import MLFlowLogger

MODEL_NAME_OR_PATH = "distilbert-base-german-cased"
device = "cpu"

tokenizer = Tokenizer.load(
    pretrained_model_name_or_path=MODEL_NAME_OR_PATH,
    do_lower_case=False
)

LABEL_LIST = ["OTHER", "OFFENSE"]
processor = TextClassificationProcessor(tokenizer=tokenizer,
                                        max_seq_len=128,
                                        data_dir="../data/germeval18",
                                        label_list=LABEL_LIST,
                                        metric="f1_macro",
                                        label_column_name="coarse_label")

BATCH_SIZE = 32
data_silo = DataSilo(processor=processor, batch_size=BATCH_SIZE)

language_model = LanguageModel.load(MODEL_NAME_OR_PATH)

prediction_head = TextClassificationHead(
    num_labels=len(LABEL_LIST), 
    class_weights=data_silo.calculate_class_weights(task_name="text_classification")
)

EMBEDS_DROPOUT_PROB = 0.1
model = AdaptiveModel(
    language_model=language_model,
    prediction_heads=[prediction_head],
    embeds_dropout_prob=EMBEDS_DROPOUT_PROB,
    lm_output_types=["per_sequence"],
    device=device
)

LEARNING_RATE = 2e-5
N_EPOCHS = 1

model, optimizer, lr_schedule = initialize_optimizer(
    model=model,
    device=device,
    learning_rate=LEARNING_RATE,
    n_batches=len(data_silo.loaders["train"]),
    n_epochs=N_EPOCHS
)

from farm.infer import Inferencer
from pprint import PrettyPrinter

infer_model = Inferencer(processor=processor, model=model, task_type="text_classification", gpu=True)

basic_texts = [
    {"text": "Martin ist ein Idiot"},
    {"text": "Martin Müller spielt Handball in Berlin"},
]
result = infer_model.inference_from_dicts(dicts=basic_texts)

print(result)

save_dir = 'model'
model.save(save_dir)