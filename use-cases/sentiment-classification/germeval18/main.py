from farm.infer import Inferencer
from farm.modeling.adaptive_model import AdaptiveModel
from farm.data_handler.processor import InferenceProcessor
import torch
from torch.cuda import is_available as is_gpu

model_dir = 'model'
device = torch.device("cuda" if is_gpu() else "cpu")

processor = InferenceProcessor.load_from_dir(model_dir)
model = AdaptiveModel.load(model_dir, device)

infer_model = Inferencer(processor=processor, model=model, task_type="text_classification", gpu=is_gpu())

basic_texts = [
    {"text": "Martin ist ein Idiot"},
    {"text": "Martin Müller spielt Handball in Berlin"},
]
result = infer_model.inference_from_dicts(dicts=basic_texts)

print(result)