from haystack import Finder
from haystack.indexing.cleaning import clean_wiki_text
from haystack.indexing.io import write_documents_to_db, fetch_archive_from_http
from haystack.reader.farm import FARMReader
from haystack.reader.transformers import TransformersReader
from haystack.utils import print_answers
from haystack.retriever.tfidf import TfidfRetriever
from haystack.database.sql import SQLDocumentStore
from flask import Flask, jsonify, request

# define data source
document_store = SQLDocumentStore(url="sqlite:///qa.db")

# receive data
url = "https://s3.eu-central-1.amazonaws.com/deepset.ai-farm-qa/datasets/documents/wiki_gameofthrones_txt.zip"
document_dir = "./data"
# fetch_archive_from_http(url, document_dir)

# populate data source
write_documents_to_db(document_store=document_store, document_dir=document_dir, clean_func=clean_wiki_text, only_empty_db=True)


reader = FARMReader(model_name_or_path="deepset/roberta-base-squad2", use_gpu=False)
retriever = TfidfRetriever(document_store=document_store)

finder = Finder(reader, retriever)

app = Flask(__name__, static_url_path='')

@app.route('/ping', methods=['GET'])
def ping():
    return "pong"

# "Who is the father of Arya Stark?"
@app.route('/ask', methods=['POST'])
def ask():
    data = request.json
    prediction = finder.get_answers(question=data.question, top_k_retriever=10, top_k_reader=5)
    
    return jsonify({
        'prediction': prediction
    })
    
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=32300, debug=True, threaded=True)