from flask import Flask, jsonify, request, Response, current_app, render_template, flash, request, redirect, url_for, send_file, send_from_directory, session
from werkzeug.utils import secure_filename
from flask_cors import CORS, cross_origin
import numpy as np
import firebase_admin
from firebase_admin import auth, credentials
from bson import ObjectId
import json
import bson
import os
import datetime
from backend.model_service import ModelService
import pathlib


class MongoEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, bson.ObjectId):
            return str(obj)
        elif isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, datetime.datetime):
            return obj.isoformat()
        else:
            return super(MongoEncoder, self).default(obj)


def bsonify(*args, **kwargs):
    indent = 0

    if current_app.config['JSONIFY_PRETTYPRINT_REGULAR'] and not request.is_xhr:
        indent = 2

    return current_app.response_class(tobson(MongoEncoder, indent, *args, **kwargs), mimetype='application/json')


def tobson(encode_clss, indent, *args, **kwargs):
    return json.dumps(dict(*args, **kwargs), cls=MongoEncoder, indent=indent).replace('\n', '')


def get_git_revision(base_path):
    git_dir = pathlib.Path(base_path) / '.git'
    with (git_dir / 'HEAD').open('r') as head:
        ref = head.readline().split(' ')[-1].strip()

    with (git_dir / ref).open('r') as git_hash:
        return git_hash.readline().strip()

UPLOAD_FOLDER = '/uploads/image-upload'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}



app = Flask(__name__, static_url_path='')
app.config['SECRET_KEY'] = '5 elements of AI'

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

API_V1 = '/api/1.0'

model_service = ModelService()

cred = credentials.Certificate("./serviceAccountKey.json")
firebase = firebase_admin.initialize_app(cred)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/uploads/image-upload/image-upload', methods=['POST'])
@cross_origin(origin='localhost')
def upload_file():
    if 'file' in request.files:
        file = request.files['file']
        file.save('./uploads/image-upload/' + file.filename)
        
        return 'upload completed ' + str(file.filename)

    return 'no file in request'
    
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return redirect(request.url)
    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return redirect(url_for('uploaded_file',
                                filename=filename))

@app.route('/', methods=['GET'])
def home():
    return render_template("index.html", title="API > 5 elements of AI", content="Hello world!")

@app.route(API_V1 + '/ping', methods=['GET'])
def ping():
    return "pong"

@app.route(API_V1 + '/info', methods=['GET'])
def info():
    user = 'Unauthenticated'
    try:
        user = auth.verify_id_token(request.headers['auth'])
    except:
        pass

    return jsonify({
        'version': API_V1,
        'project': '5 elements of AI',
        'service': 'backend',
        'language': 'python',
        'type': 'api',
        'date': str(datetime.datetime.now()),
        'user': user,
        'git': get_git_revision('.'),
    })


@app.route(API_V1 + '/use-cases', methods=['GET'])
def list_use_cases():
    items = model_service.findAll()

    return bsonify({
        'items': items,
        'meta': {
            'count': len(items)
        }
    })


@app.route(API_V1 + '/use-cases', methods=['POST'])
def create_use_case():
    return bsonify()


@app.route(API_V1 + '/use-cases/<use_case_id>', methods=['GET'])
def show_use_case(use_case_id: str):
    model = model_service.find(use_case_id)()

    return bsonify({
    })

@app.route(API_V1 + '/use-cases/<use_case_id>/predict', methods=['POST', 'OPTIONS'])
@cross_origin(origin='localhost')
def use_case_prediction(use_case_id: str):
    model = model_service.find(use_case_id)
    prediction = model.predict(request.json)
    
    print('prediction ', prediction)
    
    return bsonify({
        'data': prediction
    })

@app.route(API_V1 + '/use-cases/<use_case_id>/ping', methods=['GET'])
def use_case_ping(use_case_id: str):
    model = model_service.find(use_case_id)

    return model.ping()

@app.route(API_V1 + '/use-cases/<use_case_id>/info', methods=['GET'])
def use_case_info(use_case_id: str):
    model = model_service.find(use_case_id)
    return bsonify(model.info())


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)