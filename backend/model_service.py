import requests
from abc import ABC, abstractmethod
from json import JSONDecodeError

class Model:
    def __init__(self, api_path):
        self.api_path = api_path
        
    def ping(self):
        response = requests.get(self.api_path + "/ping")
        return response.text
        
    def info(self):
        response = requests.get(self.api_path + "/info")
        return response.json()
        
    def predict(self, data):
        api_path = self.api_path + "/predict"
        print('REQUEST: predict data on path ', api_path, ' with data ', data)
        response = requests.post(api_path, json=data)
        
        print('RESPONSE: ', response.raw)
        try:
            return response.json()
        except JSONDecodeError:
            return { 'error': response.raw }
            

class ModelService:
    def __init__(self):
        '''
        self.models = {
            'semantic-segmentation': Model("http://image-segmentation:5000/api/1.0"),
            'question-answering': Model("http://question-answering:5000/api/1.0"),
            'sentiment-classification': Model("http://sentiment-classification:5000/api/1.0"),
        }
        '''
        self.models = {
            'semantic-segmentation': Model("https://api.5elements.ai/image-segmentation/api/1.0"),
            'question-answering': Model("https://api.5elements.ai/question-answering/api/1.0"),
            'sentiment-classification': Model("https://api.5elements.ai/sentiment-classification/api/1.0"),
        }

    def find(self, id):
        return self.models.get(id)
        
    def findAll(self):
        return self.models